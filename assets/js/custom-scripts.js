$(document).ready(function(){
	
	$('.menu-toggle a').on('click', function(e){
		e.preventDefault();
		$('.navbar').toggleClass('collapsed');
		
	});
	
	
	$('input.dateinput').datepicker({
		format: "dd/mm/yyyy",
		calendarWeeks: true
	});
	
	
});